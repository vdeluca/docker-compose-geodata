# BorderGIS
<b>
Stack de producci�n para BorderGIS
</b>

## docker-compose for BorderGIS production deploy
Para que este yml funcione correctamente se debe descargar el fuente de geodata/bordergis_docker en el directorio ./bordergis

`$ cd bordergis`

`$ git clone git@bitbucket.org:vdeluca/geodata.git .`

## Dev Environment
### Folders
* <b>venv: </b>
Se puede crear un entorno de desarrollo para editar el fuente desde un ide como pycharm. Se debe modificar la versi�n del GDAL seg�n la que est� instalada en el SO.
La conexi�n con la base de datos debe ser configurada desde el local_settings.py
* <b>templates: </b>
Esta carpeta contiene los html, css y demas archivos del template. Los estilos y los js est�n dentro de `./static/dist/`


### PostGIS
Se instala directamente desde la imagen postgis/postgis. Solo se agregan las mismas credenciales que en geodata/local_settings.py

### Django
Se buildea desde ./Dockerfile, basado en python:3. Se instalan las dependencias, apt y pip, se hace copia de archivos varios, entre ellos el fuente.

### [TODO]
* rootless access
* Se debe acceder al container django para terminar de instalar el sistema:
    * `python manage.py migrate`
    * `python manage.py loaddata dump.json`
    * `python manage.py runserver 0.0.0.0:8000`
    * Crear la carpeta `./static/tmp_json`


El puerto esta mapeado para ser consultado desde el 80
