FROM python:3.7
ENV PYTHONBUFFERED=1
RUN \
      apt update && \
      apt install python3-pyproj python3-dev gdal-bin git python-numpy \ 
      virtualenv libgdal-dev python3-psycopg2 libgdal20 python3-lxml libxml2-dev python3-gdal vim -y 
WORKDIR /root
COPY .bashrc .
WORKDIR /usr/src/app
COPY bordergis/requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY bordergis/. .
COPY bordergis/geodata/local_settings_sample.py ./geodata/local_settings.py
